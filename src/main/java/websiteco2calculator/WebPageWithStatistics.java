package websiteco2calculator;

public final class WebPageWithStatistics {

    private final Bytes pageSizeBytes;
    private final KiloWattGrammPerByte kiloWattGrammPerByte;
    private final CO2 co2Renewable;
    private final CO2 co2Grid;
    private final JsonConverter jsonConverter;

    public WebPageWithStatistics(Bytes pagesize, KiloWattGrammPerByte energyConsumption, CO2 renew, CO2 grid) {
        this.pageSizeBytes = pagesize;
        this.kiloWattGrammPerByte = energyConsumption;
        this.co2Renewable = renew;
        this.co2Grid = grid;
        this.jsonConverter = new JsonConverterUsingGsonImpl();
    }

    private WebPageWithStatistics(Bytes pageSizeBytes, KiloWattGrammPerByte kiloWattGrammPerByte, CO2 co2Renewable,
                                  CO2 co2Grid, JsonConverter c) {
        this.pageSizeBytes = pageSizeBytes;
        this.kiloWattGrammPerByte = kiloWattGrammPerByte;
        this.co2Renewable = co2Renewable;
        this.co2Grid = co2Grid;
        this.jsonConverter = c;

    }

    public Bytes getPagesize() {
        return pageSizeBytes;
    }

    public KiloWattGrammPerByte getEnergyConsumption() {
        return kiloWattGrammPerByte;
    }

    public CO2 getCO2Renewable() {
        return co2Renewable;
    }

    public CO2 getCO2Grid() {
        return co2Grid;
    }

    public WebPageWithStatistics withJsonConverter(JsonConverter c) {
        if (c != null) {
            return new WebPageWithStatistics(pageSizeBytes, kiloWattGrammPerByte, co2Renewable, co2Grid, c);
        }
        return this;
    }

    @Override
    public String toString() {
        return this.jsonConverter.toJson(this);
    }

}
