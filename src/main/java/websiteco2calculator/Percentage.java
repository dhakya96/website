package websiteco2calculator;

public final class Percentage {

    private final Double value;

    private Percentage(Double value) {
        if (value < 0.0) {
            throw new IllegalArgumentException("must be greater than 0");
        }
        if (value > 1.0) {
            throw new IllegalArgumentException("must be smaller than 100");
        }
        this.value = value;
    }

    public static Percentage of(double d) {
        return new Percentage(Double.valueOf(d));
    }

    public static Percentage of(int d) {
        double i = d / 100.0;
        return new Percentage(Double.valueOf(i));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        long temp = Double.doubleToLongBits(value);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        Percentage other = (Percentage) obj;
        return value.equals(other.value);
    }

    public Integer asInt() {
        return (int) Math.round(this.value * 100);
    }

    public Double asDouble() {
        return this.value;
    }

    @Override
    public String toString() {
        return asInt() + "%";
    }
}
