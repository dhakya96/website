package websiteco2calculator;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.*;
import static websiteco2calculator.WebPage.*;

class WebPageTest {

    @Test
    void create() {
        assertNotNull(WebPage.withPageSize(Bytes.B(1)));
    }

    @Test
    void invalidValue() {
        assertThrows(IllegalArgumentException.class, () -> WebPage.withPageSize(null));
    }

    @Test
    void RETURNING_VISITOR_PERCENTAGE() {
        assertEquals(Percentage.of(0.75), RETURNING_VISITOR_PERCENTAGE);
    }

    @Test
    void DATA_LOADED_ON_SUBSEQUENT_LOAD() {
        assertEquals(Percentage.of(0.02), DATA_LOADED_ON_SUBSEQUENT_LOAD);
    }

    @Test
    void FIRST_TIME_VIEWING_PERCENTAGE() {
        assertEquals(Percentage.of(0.25), FIRST_TIME_VIEWING_PERCENTAGE);
    }

    @ParameterizedTest
    @CsvSource({
            "0,0",
            "1000,755"
    })
    void adjustDataTransferRateFromPageSize(long in, long expected) {
        Bytes input = Bytes.B(in);

        Bytes adjustedDataTransferRate = createWebPage(input).getAdjustedDataTransferRate();

        assertEquals(expected, adjustedDataTransferRate.asLong());
    }

    @Test
    void adjustDataTransferRateFromPageSizeCalculation() {
        Bytes input = Bytes.B(1000);

        Bytes adjustedDataTransferRate = createWebPage(input).getAdjustedDataTransferRate();

        assertNotNull(adjustedDataTransferRate);
        double calcResult = (input.asLong() * RETURNING_VISITOR_PERCENTAGE.asDouble()) + (input.asLong() * DATA_LOADED_ON_SUBSEQUENT_LOAD.asDouble() * FIRST_TIME_VIEWING_PERCENTAGE.asDouble());
        long expected = Math.round(calcResult);
        assertEquals(expected, adjustedDataTransferRate.asLong());
    }

    @Test
    void calculateWebPageStats() {
        Bytes bytes = Bytes.MB(4.2);

        WebPageWithStatistics stats = createWebPage(bytes).calculateCO2();

        assertNotNull(stats);
        assertEquals(bytes, stats.getPagesize());
    }

    private static WebPage createWebPage(Bytes bytes) {
        return WebPage.withPageSize(bytes);
    }
}
