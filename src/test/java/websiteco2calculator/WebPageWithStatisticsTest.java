package websiteco2calculator;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;


public class WebPageWithStatisticsTest {
	
	private final class JsonConverterImplementation implements JsonConverter {
		Object calledWith = null;
		@Override
		public String toJson(Object o) {
			this.calledWith = o;
			return "called";
		}
	}

	@Test
	void create() {
		int pageSize = 1;
		int energyConsumption = 2;
		int co2Renewable = 3;
		int co2Grid = 3;
		WebPageWithStatistics stats = new WebPageWithStatistics(
			Bytes.B(pageSize)
			, KiloWattGrammPerByte.of(energyConsumption)
			, new CO2(co2Renewable)
			, new CO2(co2Grid)
		);
		assertEquals(pageSize, stats.getPagesize().asLong());
		assertEquals(energyConsumption, stats.getEnergyConsumption().asDouble());
		assertEquals(co2Renewable, stats.getCO2Renewable().asDouble());
		assertEquals(co2Grid, stats.getCO2Grid().asDouble());
	}

	@Test
	void toString_shouldProduceJson() {
		JsonConverterImplementation testConverter = new JsonConverterImplementation();
		WebPageWithStatistics stats = someWebStatistics().withJsonConverter(
			testConverter
		);
		
		stats.toString();

		assertEquals(stats, testConverter.calledWith);
	}

	private WebPageWithStatistics someWebStatistics() {
		return new WebPageWithStatistics(Bytes.B(1), KiloWattGrammPerByte.of(1), new CO2(1), new CO2(1));
	}

}
