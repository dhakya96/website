package websiteco2calculator;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class CO2UsagePercentilesTest {

    @Test
    void getPercentiles() {
        assertNotNull(CO2UsagePercentiles.getPercentiles());
        assertEquals(100, CO2UsagePercentiles.getPercentiles().size());
    }

}
